# [Propensity to Fund Mortgages Competition](https://www.crowdanalytix.com/contests/propensity-to-fund-mortgages)
## Main Goal
Main goal of this competition was to predict funded and not funded mortgages based on simple data (one row for each customer). I used **F1 macro score** to evaluate model's accuracy.
## Data
- **Property Value** - The appraised value of the property
- **Mortgage Payment** - A mortgage payment amount comprised of a principal portion, an interest portion, a property tax portion and a life insurance portion, calculated based on the interest rate, amortization period and payment frequency.
- **GDS**	- The measure of the percentage of Gross Annual Income required, including rental income if applicable, for Housing Expenses (principal & interest, property taxes, heat, 50% of condominium fees) used for qualification. GDS % = Annualized Housing Expenses / Annual Income
- **LTV** -	Loan-to-value (LTV) is the measure of the Mortgage Amount to the Property Value. LTV = Mortgage Amount / Appraised Property Value
- **TDS** -	The measure of the percentage of Gross Annual Income, including rental income if applicable, required for Housing Expenses (principal & interest, property taxes, heat, 50% of condominium fees) and other consumer debt obligations ("Other Expenses") used for qualification. TDS % = (Annualized Housing Expenses + Other Expenses) / Annual Income
- **Amortization** - The requested amortization in months.
- **Mortgage Amount** -	The requested mortgage amount in dollars.
- **Rate** -	The interest rate used to qualify the mortgage application.
- **Mortgage Purpose** - The purpose for which the applicant(s) is/are requesting the mortgage, purchase or refinance.
- **Payment Frequency** - The frequency with which the applicant makes regular mortgage payments.
- **Property Type** -	The classification of the property being used to secure the mortgage.
- **Term** - The requested mortgage term in months.
- **FSA** -	The Forward Sortation Area (FSA) is the first three characters of the postal code, designating a postal delivery area of property being mortgaged.
- **Age Range** -	The age range of the main applicant.
- **Gender**	The gender of the main applicant.
- **Income** - Total amount of income being used to qualify for the mortgage.
- **Income Type** -	Numericalized employment status type for the income
- **NAICS Code** -	Job Category by NAICS (North America Industry Classification System) code of the main applicant.
- **Credit Score** -	AKA Beacon Score - A number generated by the Equifax Credit Bureau for the main applicant’s credit-worthiness at the time of underwriting review and how likely it is that the applicant(s) will repay. Mathematical criteria involved in calculating a Beacon score can include late payments, current debts, length of time an account has been open, types of credit and new applications for credit.
- **Result**	The result of the application, either funded or not funded

## Feature Engineering
### Postal Code
I decided to create "FSA GENERAL" feature which contains just first letter of postal code which allow model to recognize in which state property is. I did because before that I had 293 unique postal codes where number of occurrences was smaller than 5. After simplification I had just 17 unique values.

### Property Type
I also decided to group types of property because some of them are similar. Primary property had 8 unique values, check below.

|Primary Property Type   | #    |
|:----------------------:|:----:|
|Single Detached         | 29377|
|High Rise               |  5909|
|Semi-Detached           |  4227|
|Townhouse Freehold      |  3040|
|Townhouse Condominium   |  2309|
|Duplex                  |   469|
|Triplex                 |   193|
|Fourplex                |   118|
Based on below dictionary I simplified to 5 unique values.
```python
property_type_mapper = {
    'Duplex': 'Multi-family building',
    'Triplex': 'Multi-family building',
    'Fourplex': 'Multi-family building',
    'Townhouse Freehold': 'Townhouse',
    'Townhouse Condominium': 'Townhouse',
}
```
After mapping results looked like below.

|Simplified Property Type| #    |
|:----------------------:|:----:|
|Single Detached         | 29377|
|High Rise               |  5909|
|Townhouse               |  5349|
|Semi-Detached           |  4227|
|Multi-family building   |   780|

## Categorical Variables Encoding
I tried two approaches:
- [label encoder](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.LabelEncoder.html),
- [target encoding](https://maxhalford.github.io/blog/target-encoding-done-the-right-way/).

I didn't try to use one hot encoding because I have to many categorical columns.

The problem with categorical values is that it don't have natural numerical representation. Label Encoder is just replacing strings by integer numbers what's not so natural and beneficial for models. Target encoding is more efficient way to encode categorical values because it's based on target column (in this situation column "RESULT"). Encoding algorithm is calculating mean of target column for each categorical value and then replace it by result. This kind of way is more natural and beneficial for models. **Middlingly target encoding increase model's f1 macro score for ~3%.**

## Variables Correlation
Based on below graph with variables correlation I decided to drop:
- GDS,
- PROPERTY VALUE,
- MOTRAGE PAYMENT.

![Correlation](./readme_source/correlation.png)

## Train / Test Data Rate
I decided to divide data in **8 : 2** ratio what's mean that I had 36513 train and 9129 test rows.

## Imbalanced Data
In train dataset 79.0% rows were funded and 21% rows were not funded. Based on this statistics I could say that this data is imbalanced. I tried a few approaches to close this gap, check below.

### Oversampling
Firstly I tried to generate new not funded rows by randomly sampling with replacement the current available samples. This approach wasn't beneficial - model was classifying everything as not funded.

### Downsampling
Secondly I tired to decrease number of funded records in train dataset, I randomly selected N funded rows where N was number of not funded records. This approach also wasn't beneficial - model's prediction was close to random.

### ROC analysis
Finally I decided to use imbalanced data and increase bound of probability from which I will decide that mortgage was funded. This approach provided the best results. I used [roc_curve](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.roc_curve.html) function to calculate false positives, true positives statistics and evaluate best threshold (bound) to mark mortgage as funded. Check [Jupyter Notebook File](main.ipynb) for more details.

## Models Fitting
### Random Forest
Firstly I decided to fit Random Forest model with following parameters
```python
random_forest_clf = RandomForestClassifier(
    n_estimators=100,
    criterion='gini',
    max_features=None,
    random_state=100
)
```
this parameters was returned as optimal by [Grid Search](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html) algorithm.

Using below code I estimated f1 macro score
```python
fpr, tpr, treshold = roc_curve(y_test, y_pred_prob_dict)
score = f1_score(
    y_test,
    list(map(lambda x: 1 if x > np.mean(treshold) else 0, y_pred_prob)),
    average='macro'
)
print(f'F1 score: {score}')
```
which was middlingly equal to 0.60. Check below graph of ROC analysis.
![random_forest_graph](./readme_source/random_forest_roc.png)

### XGBoost
Secondly I tried to fit XGBoost model with following paremeters
```python
xgb_clf = xgb.XGBClassifier(
    objective ='binary:logistic',
    subsample=0.55,
    n_estimators=298,
    max_depth=15,
    learning_rate=0.1,
    colsample_bytree=1.0,
    alpha=14
)
```
this parameters were returned as optimal by [Random Search](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.RandomizedSearchCV.html). This time I used Random Search because it's better way to estimate optimal numerical parameters ([more details](http://www.jmlr.org/papers/volume13/bergstra12a/bergstra12a.pdf)).

Using the same code as in Random Forest I estimated f1 macro score and it was also close to 0.60.
![random_forest_graph](./readme_source/xgboost_roc.png)

### CatBoost
Last model which I tried to fit was CatBoost model with default parameters. Estimated f1 macro score was close to 0.62, check ROC graph below.
![random_forest_graph](./readme_source/catboost_roc.png)

## Best Model
Finally I submitted predictions from each of below models, the best result had CatBoost model ( 0.60908 f1 macro score). I had 155th result of almost 300 participants.
